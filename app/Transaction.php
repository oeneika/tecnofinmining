<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * Table
     */
    protected $table = 'transactions';

    /**
     * Primary Key
     */
    protected $primaryKey = 'transaction_id';

    /**
     * Get Payment Method
     */
    public function paymentMethod(){
        return $this->hasOne('App\PaymentMethod','payment_method_id');
    }

    /**
     * Get User
     */
    public function user(){
        return $this->hasOne('App\User','user_id');
    }
}
