<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use View;

class GlobalDataProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $total_btc = \App\Wallet::selectRaw('SUM(quantity) as total_btc')
        ->first();
        $total_btc = $total_btc->total_btc;

        $total_transactions = \App\Transaction::selectRaw('COUNT(id_transaction) as total_transactions')
        ->first();
        $total_transactions = $total_transactions->total_transactions;

        View::share('total_btc',$total_btc);
        View::share('total_transactions',$total_transactions);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
