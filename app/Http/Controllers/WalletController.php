<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

class WalletController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'address'=>'required|between:26,35|alpha_num|unique:wallet,address',
            'label'=>'max:180',
            'id_user'=>'exists:users,id',
        ]);

        if ($validator->passes()){

            $id_user = 0;

            if(!Auth::user()){
                $user = \App\User::find($request->input('id_user'));

                if($user->wallets){
                    return response()->json([
                        'success'=>0,
                        'errors'=>[
                            0=>'El usuario ya tiene una wallet registrada'
                        ]]);
                }

                $id_user = $user->id;
            }
            else{
                $id_user = Auth::user()->id;
            }

            $wallet = new \App\Wallet();
            $wallet->id_user = $id_user;
            $wallet->address = $request->input('address');
            $wallet->label = $request->input('label');

            $wallet->save();

            return response()->json(array('success'=>1,'message'=>'Wallet agregada con éxito'));
        }

        return response()->json(array('success'=>0,'errors'=>$validator->errors()->all()));
    }
}
