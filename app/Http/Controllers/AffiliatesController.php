<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AffiliatesController extends Controller
{
    /**
     * Show list of affiliate comerces/ Muestra lista de comercios afiliados
     */
    public function index()
    {
        $affiliates = \App\Affiliate::all();
        return view('affiliates.affiliates',[
            'affiliates'=>$affiliates,
        ]);
    }

    /**
     * Store an affiliate
     */
    public function store(Request $request){
        $request->validate([
            'rif'=>'required|max:100|unique:affiliates,rif',
            'branch'=>'required|max:255',
            'name'=>'required|max:255',
            'address'=>'required|max:255',
            'phone'=>'required|alpha_num|max:255',
        ]);

        $affiliate = new \App\Affiliate;

        $affiliate->rif = $request->input('rif');
        $affiliate->branch = $request->input('branch');
        $affiliate->name = $request->input('name');
        $affiliate->address = $request->input('address');
        $affiliate->phone = $request->input('phone');

        $affiliate->save();

        return redirect()->back()->with('Success','Creado con éxito');
    }

    /**
     * Update an affiliate/Actualiza un afiliado
     */
    public function update(Request $request){

        #Valida campos
        $request->validate([
            'id_affiliate' => 'required'
        ]);

        $id = $request->input('id_affiliate');

        $request->validate([
            'rif'=>'required|max:100|unique:affiliates,rif,'.$id.',id_affiliate',
            'branch'=>'required|max:255',
            'name'=>'required|max:255',
            'address'=>'required|max:255',
            'phone'=>'required|alpha_num|max:255'
        ]);

        $affiliate = \App\Affiliate::find($id);

        $affiliate->rif = $request->input('rif');
        $affiliate->branch = $request->input('branch');
        $affiliate->name = $request->input('name');
        $affiliate->address = $request->input('address');
        $affiliate->phone = $request->input('phone');

        $affiliate->save();

        return redirect()->back()->with(['Success'=>'Editado con éxito']);
    }

    /**
     * Delete Affiliate
     */
    public function destroy($id){
        $affiliate = \App\Affiliate::find($id);
        $affiliate->delete();

        return response()->json(['success'=>1, 'message'=>'Eliminado con éxito']);
    }
}
