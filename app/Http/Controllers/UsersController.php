<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    /**
     * Shows all users/Muestra todos los usuarios
     */
    public function index()
    {
        $users = \App\User::where('role',0)
        ->where('inactive',1)
        ->get();
        return view('users/users',[
            'users' => $users,
        ]);
    }

    public function indexperfil()
    {
        return view('profile/profile');
    }

    /**
     * Store an user/Guarda un usuario
     */
    public function store(Request $request){

        #Valida campos
        $request->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'username' => 'required|alpha_dash|string|max:255|unique:users,user',
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'cedula'=>'required|alpha_num|unique:users,ci',
            'password' => 'required|string|min:6|confirmed',
            'saldo'=>'numeric',
            'email'=>'required|email|max:255|unique:users,email'
        ]);

        $user = new \App\User;

        #Saves user
        $user->name = $request->input('name');
        $user->user = $request->input('username');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->ci = $request->input('cedula');
        $user->saldo = $request->input('saldo');
        $user->password = Hash::make($request->input('password'));
        $user->role = 0;

        $user->save();

        #Create Wallets
        $coins = \App\Coin::all();

        foreach ($coins as $key => $value) {
            $wallet = new \App\Wallet;
            $wallet->address = str_limit( Hash::make( (string) Str::uuid() ), 37, '' );
            $wallet->id_user = $user->id;
            $wallet->quantity = 0;
            $wallet->label = $value->abbreviation;
            $wallet->id_coin = $value->id_coin;
            $wallet->save();
        }

        return redirect()->back()->with(['Success'=>'Creado con éxito']);
    }

    /**
     * Store an user/Guarda un usuario
     */
    public function update(Request $request){

        #Valida campos
        $request->validate([
            'id_user' => 'required'
        ]);

        $id = $request->input('id_user');

        $request->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'username' => 'required|alpha_dash|string|max:255|unique:users,user,'.$id,
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'cedula'=>'required|alpha_num|unique:users,ci,'.$id,
            'email'=>'required|email|max:255|unique:users,email,'.$id,
            'password'=>'nullable|string|min:6|confirmed',
            'saldo'=>'sometimes|numeric',
        ]);

        $user = \App\User::find($id);

        $user->name = $request->input('name');
        $user->user = $request->input('username');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->ci = $request->input('cedula');
        $user->saldo = $request->input('saldo');

        //If the password was filled
        if($request->input('password')){
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return redirect()->back()->with(['Success'=>'Editado con éxito']);
    }

    /**
     * Delete user/Borra usuario
     */
    public function destroy($id){
        $user = \App\User::find($id);
        $user->inactive = 0;
        $user->save();

        return response()->json(['success'=>1, 'message'=>'Eliminador con éxito']);
    }

    /**
     * Edit authenticated user
     */
    public function editProfile(Request $request, $id){
        $validator  = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'username' => 'required|alpha_dash|string|max:255|unique:users,user,'.$id,
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'email'=>'required|email|max:255|unique:users,email,'.$id,
            'password'=>'nullable|string|min:6|confirmed',
        ]);

        #If validator fails
        if($validator->fails()){
            return response()->json(['success'=>0,'errors'=>$validator->errors()->all()]);
        }

        $user = \App\User::find($id);

        $user->name = $request->input('name');
        $user->user = $request->input('username');
        $user->lastname = $request->input('lastname');
        $user->phone = $request->input('phone');
        $user->gender = $request->input('gender');
        $user->mobile = $request->input('mobile');
        $user->email = $request->input('email');
        $user->country = $request->input('country');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->address = $request->input('address');
        $user->birthday = $request->input('birthday');

        if($request->input('password')){
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return response()->json(['success'=>1,'message'=>'Perfil editado con éxito']);
    }

    /**
     * Returns json with last 5 transactions and total btc
     */
    public function generalInfo($id){

        #If validation fails
        if(!e($id)){
            return response()->json(['success'=>0,'errors'=>[
                0=>'el id es obligatorio'
            ]]);
        }

        $user = \App\User::find($id);
        $wallets = \App\Wallet::where('id_user',$id)->get();

        if($user){
            $total_btc = $user;
            // $transactions = $user->transactions->sortBy('updated_at')->take(5)->all();
            $transactions = \App\Transaction::where('id_buyer_user',$id)
            ->join('currency','currency.id_currency','=','transaction.id_currency')->get()->take(5);

            return response()->json([
                'user' => $user,
                'wallets' => $wallets,
                'transactions'=> $transactions,
            ]);
        }
    }

}
