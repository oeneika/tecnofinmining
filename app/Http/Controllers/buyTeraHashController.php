<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Auth;
use Validator;

class BuyTeraHashController extends Controller
{
    /**
     * Display Buy Tera Hash screen
     */
    public function index()
    {
        return view('buyTeraHash/buyTeraHash',[
            'methods'=> \App\PaymentMethod::all(),
        ]);
    }

}
