<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfilesController extends Controller
{
    /**
     * Display profile
     */
    public function index(){
        return view('profile/profile');
    }

    /**
     * Edit authenticated user
     */
    public function edit(Request $request, $id){
        $request->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'username' => 'required|alpha_dash|string|max:255|unique:users,username,'.$id,
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'phone'=>'required|alpha_num',
            'email'=>'required|email|max:255|unique:users,email,'.$id
        ]);

        $user = \App\User::find($id);

        $user->name = $request->input('name');
        $user->username = $request->input('username');
        $user->lastname = $request->input('lastname');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');

        $user->save();

        return redirect()->back()->with(['Success'=>'Editado con éxito']);
    }
}
