<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;

class AdminController extends Controller
{
    public function index()
    {
        $admins = \App\User::where('role',1)
        ->where('inactive',1)->get();
        
        return view('administradores/administradores',[
            'admins'=>$admins,
        ]);
    }

    /**
     * Store an admin/Guarda un admin
     */
    public function store(Request $request){

        #Valida campos
        $request->validate([
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'username' => 'required|alpha_dash|string|max:255|unique:users,user',
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'cedula'=>'required|alpha_num|unique:users,ci',
            'saldo'=>'numeric',
            'email'=>'required|email|max:255|unique:users,email'
        ]);

        $user = new \App\User;

        $user->name = $request->input('name');
        $user->user = $request->input('username');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->ci = $request->input('cedula');
        $user->saldo = $request->input('saldo');
        $user->password = Hash::make($request->input('password'));
        $user->role = 1;

        $user->save();

        return redirect()->back()->with('Success','Creado con éxito');
    }
}
