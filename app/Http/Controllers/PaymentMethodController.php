<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    /**
     * Returns all payment methods
     */
    public function get(){

        return response()->json(\App\PaymentMethod::all());

    }
}
