<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Validator;
use Illuminate\Support\Str;

class PublicAuthController extends Controller
{
    /**
     * 
     */
    public function login(Request $request){

        #Validate inputs
        $validator = Validator::make($request->all(), [
            'email'=>'required',
            'password'=>'required',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>0,'errors'=>$validator->errors()->all()]);
        }

        $user = \App\User::where('email',$request->input('email'))->first();

        if( $user && Hash::check($request->input('password'),$user->password) ){
            return  response()->json([
                'success'=>1,
                'message'=>'Sesión iniciada con éxito',
                'user'=>$user,
                ]);
        }

        return response()->json(['success'=>1,'errors'=>[0=>'Credenciales incorrectas']]);
        
    }

    /**
     * Api function for registration
     * 
     * @param Request $request
     * 
     * @return Illimante\Http\Response
     */
    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255|unique:users',
            'username' => 'required|alpha_dash|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>0,'errors'=>$validator->errors()->all()]);
        }

        $user = new \App\User;

        $user->name = $request->input('name');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->username = $request->input('username');
        $user->password = Hash::make($request->input('password'));

        $user->save();

        $wallet = new \App\Wallet();

        $wallet->user_id = $user->id;
        $wallet->address = str_limit( Hash::make( (string) Str::uuid() ), 37, '' );
        $wallet->label = 'BTC';
        
        $wallet->save();

        return response()->json(['success'=>1, 'message'=>'Registrado con éxito']);
    }

    /**
     * Api function to update profile
     * 
     * @param Request $request
     * 
     * @return Illimante\Http\Response
     */
    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'lastname' => 'required|regex:/^[\pL\s\-]+$/u|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            'phone' => 'required|string|max:255',
            'username' => 'required|alpha_dash|max:255|unique:users,username,'.$id,
            'password' => 'nullable|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json(['success'=>0,'errors'=>$validator->errors()->all()]);
        }

        $user = \App\User::find($id);

        $user->name = $request->input('name');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->username = $request->input('username');

        if($request->input('password')){
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        return response()->json(['success'=>1, 'message'=>'Actualizado con éxito']);
    }
}
