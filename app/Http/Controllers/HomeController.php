<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\BitcoinPrice;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $btc = new BitcoinPrice();

        return view('home',[
            'BTC'=>$btc->getPrice(),
        ]);
    }

    /**
     * Return BTC price to api
     */
    public function btcPrice(){
        $btc = new BitcoinPrice();
        return response()->json($btc->getPrice()['bpi']['USD']['rate_float']);
    }

    /**
     * Graph data
     */
    public function hashRevenue(){

        $payouts = \App\Payout::selectRaw('SUM(payout) as monthly_payout, MONTHNAME(created_at) as month')
        ->where('user_id',Auth::user()->id)
        ->whereRaw('PERIOD_DIFF(CURRENT_DATE,created_at) < 13')
        ->groupBy('month')
        ->get();   

        return response()->json($payouts);
    }

    /**
     * Terahash simulation
     */
    public function simulation(){
        
        #Get user
        $user = Auth::user();

        #Current th balance of authenticated user
        $current_th = $user->terahash_balance;
        #Add or substract a random amount beetween 0 and the 2% of the current th
        $new_th = -($current_th*0.02) + mt_rand() / mt_getrandmax() * (($current_th*0.02) - -($current_th*0.02));
        $user->terahash_balance = $current_th + $new_th;

        $user->save();

        return response()->json(['terahash'=>$user->terahash_balance]);
        
    }
}
