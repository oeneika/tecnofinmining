<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get user info
     */
    public function show($id){
        $user = \App\User::find($id);
        $wallet = \App\Wallet::where('user_id',$id)->first();
        return response()->json([
            'user'=>$user,
            'wallet'=>$wallet,
        ]);
    }

    /**
     * Get transactions
     */
    public function getTransactions($id){
        $transactions = \App\Transaction::where('user_id',$id)->orderBy('created_at','desc')->get();
        $payouts = \App\Payout::where('user_id',$id)->get();

        return response()->json([
            'transactions'=>$transactions,
            'payouts'=>$payouts,
        ]);
    }
}
