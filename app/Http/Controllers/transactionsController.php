<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

class TransactionsController extends Controller
{
    public function buyHistory()
    {
        $transactions = \App\Transaction::all();
        return view('transactions/buy',[
            'buys'=>$transactions,
        ]);
    }

    public function sellHistory()
    {
        $transactions = \App\Transaction::where('payment_method_id',4)->get();
        return view('transactions/sell',[
            'sells'=>$transactions,
        ]);
    }
    
    /**
     * Current BTC price
     */
    private function btcPrice(){
        #Search for the last price of BTC
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://api.coindesk.com/v1/bpi/currentprice.json");
        $result = curl_exec($ch);

        curl_close($ch);

        $result = json_decode($result, true);

        return $result['bpi']['USD']['rate_float'];
    }

    /**
     * Validate errors
     */
    private function validateErrors(Request $request, float $btc){

        $validation = Validator::make($request->all(),[
            'amount'=>'required|numeric',
            'payment_method'=>'required|numeric|exists:payment_methods,payment_method_id',
        ]);

        $validation->sometimes('user_id','required',function ($input) {
            return !Auth::check();
        });

        

        #Validate if balance is less than what you want to buy
        $validation->after(function( $validation ) use($request,$btc) {
            if ( $request->payment_method == 4 ){
                $balance = Auth::user()->wallet->current_balance;
                if ( $balance < ( $request->amount / $btc ) ){
                    
                    $validation->errors()->add('balance','El balance es menor a lo que quiere comprar');

                }
            }
        });

        return $validation;

    }

    /**
     * Store a buy terahash transaction
     * 
     * @param Request $request
     */
    public function store(Request $request){

        #We get the current price of BTC
        $btc = $this->btcPrice();

        #First we validate!
        $validation = $this->validateErrors($request,$btc);

        if ( $validation->fails() ){
            return response()->json([
                'success'=>0,
                'errors'=>$validation->errors()->all(),
            ]);
        }

        #Then we create!
        $user_id = Auth::check() ? Auth::user()->id : $request->user_id ;
        $transaction = new \App\Transaction();
        $user = \App\User::find($user_id);

        $terahash_quantity = $request->amount / 75;
        $transaction->payment_method_id = $request->payment_method;
        $transaction->amount = $request->amount;
        $transaction->terahash_quantity = $terahash_quantity;
        $transaction->user_id = $user_id;
        $user->terahash_balance += $terahash_quantity;

        #Execute Payment
        $this->pay($request,$btc);


        $transaction->save();
        $user->save();

        return response()->json([
            'success'=>1,
            'message'=>'¡Terahash comprado con éxito!',
        ]);

    }

    /**
     * Execute payment 
     * @param Request $request
     * @param float $btc : Current [rice of btc]
     */
    private function pay(Request $request, float $btc){

        if ( $request->payment_method == 4 ){
            $wallet = \App\Wallet::find(Auth::user()->wallet->wallet_id);
            $wallet->current_balance -= ( $request->amount / $btc );
            $wallet->save();
        }

    }
}
