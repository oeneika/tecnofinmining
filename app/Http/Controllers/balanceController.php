<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\BitcoinPrice;

class BalanceController extends Controller
{
    public function index()
    {
        $btc = new BitcoinPrice();
        return view('balance/balance',[
            'BTC' => $btc->getPrice(),
            ]
        );
    }
}
