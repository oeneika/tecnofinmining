<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CreditController extends Controller
{

    /**
     * Display credit page
     */
	public function index(){
        $users = \App\User::where('role',0)
        ->where('inactive',1)
        ->get();
        $coins = \App\Coin::all();
        return view('credit/credit',[
            'users' => $users,
            'coins' => $coins,
        ]);
    }

    /**
     * Update user blance
     */
    public function update(Request $request){

        $request->validate([
            'user' => 'required|exists:users,id',
            'quantity' => 'required|numeric',
            'coin' => 'required|numeric',
        ]);

        $wallet = \App\Wallet::where('id_user',$request->input('user'))
        ->where('id_coin',$request->input('coin'))->first();

        $user = \App\User::where('id',$request->input('user'))->first();

        $wallet->quantity += $request->input('quantity');

        $wallet->save();

        return redirect()->back()->with('Success','Abonado '. $request->input('quantity') .' '. $wallet->coin->abbreviation .' al usuario ' . $user->user );
    }
    
}
