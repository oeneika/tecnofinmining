<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\BitcoinPrice;
use App\Classes\EthereumPrice;

class MiscController extends Controller
{
    public function cryptoPrices(){
        return [
            'bitcoin'=> ( ( new BitcoinPrice )->getPrice() )['bpi']['USD']['rate_float'],
            'ethereum'=> floatval( ( ( new EthereumPrice )->getPrice() )[0]['price_usd'] ),
        ];
    }
}
