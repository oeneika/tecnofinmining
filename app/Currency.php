<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * Table
     */
    protected $table = 'currency';

    /**
     * Primary key
     */
    protected $primaryKey = 'id_currency';

    /**
     * No timestamps
     */
    public $timestamps = false;
}
