<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            #Get all transactions
            $transactions = \App\Transaction::all();

            #Search for the last price of BTC
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, "https://api.coindesk.com/v1/bpi/currentprice.json");
            $result = curl_exec($ch);

            curl_close($ch);

            $result = json_decode($result, true);

            foreach ($transactions as $key => $transaction) {

                # Calculate how much time has passed
                $time_passed =  date_diff(date_create($transaction->created_at), date_create());
                dump($time_passed->days);
                #If a month has passed 
                if( $time_passed->days % 31 == 0 ) {
                    $wallet = \App\Wallet::where('user_id',$transaction->user_id)->first();
                    $payout = new \App\Payout();

                    #Calculate revenue
                    $revenue = $transaction->terahash_quantity * 0.06 / $result['bpi']['USD']['rate_float'];
                    dump("Revenue : $revenue");

                    $wallet->current_balance += $revenue;
                    $wallet->historic_balance += $revenue;

                    $payout->user_id = $transaction->user_id;
                    $payout->payout = $revenue;

                    $payout->save();
                    $wallet->save();
                }
                
            }
        })->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
