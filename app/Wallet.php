<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    /**
     * Table
     */
    protected $table = 'wallets';

    /**
     * Primary Key
     */
    protected $primaryKey = 'wallet_id';

    /**
     * Timestamps
     */
    public $timestamps = false;
}
