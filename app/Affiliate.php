<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affiliate extends Model
{
    /**
     * Database Table / Tabla de la base de datos
     */
    protected $table = 'affiliates';

    /**
     * Primary Key/Clave Primaria
     */
    protected $primaryKey = 'id_affiliate';

    /**
     * No timestamp
     */
    public $timestamps = false;
}
