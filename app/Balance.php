<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    /**
     * Table
     */
    protected $table = "balance";

    protected $primaryKey = "balance_id";

    public $timestamps = false;
}
