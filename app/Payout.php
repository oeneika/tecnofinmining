<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
    /**
     * Table
     */
    protected $table = 'payouts';

    /**
     * Primary Key
     */
    protected $primaryKey = 'payout_id';
}
