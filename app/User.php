<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname', 'email', 'password','username','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get Transactions
     */
    public function transactions(){
        return $this->hasMany('App\Transaction','user_id','id');
    }

    /**
     * Get Balance
     */
    public function balance(){
        return $this->hasOne('App\Balance','user_id');
    }

    /**
     * Get wallet
     */
    public function wallet(){
        return $this->hasOne('App\Wallet','user_id');
    }
}
