<?php

namespace App\Classes;

class EthereumPrice {

    private $price;

    public function __construct(){
        #Search for the last price of BTC
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, "https://api.coinmarketcap.com/v1/ticker/ethereum/");
        $result = curl_exec($ch);

        curl_close($ch);

        $this->price = json_decode($result, true);
    }

    public function getPrice(){
        return $this->price;
    }
        

}