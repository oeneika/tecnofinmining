<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    /**
     * Database Table / Tabla de la base de datos
     */
    protected $table = 'coins';

    /**
     * Primary Key/Clave Primaria
     */
    protected $primaryKey = 'id_coin';

    /**
     * No timestamp
     */
    public $timestamps = false;
}
