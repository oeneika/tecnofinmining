<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmationTokenEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Confirmation Token
     */
    protected $confirmation_token;

    /**
     * Transaction number
     */
    protected $transaction_number;

    /**
     * Username
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($confirmation_token, $user, $transaction_number)
    {
        $this->confirmation_token = $confirmation_token;
        $this->transaction_number = $transaction_number;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails/confirmationTokenMail')->with([
            'user'=>$this->user,
            'transaction_number'=>$this->transaction_number,
            'confirmation_token'=>$this->confirmation_token,
        ]);
    }
}
