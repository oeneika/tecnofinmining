<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    /**
     * Table
     */
    protected $table = 'payment_methods';

    /**
     * Primary Key
     */
    protected $primaryKey = 'payment_methods_id';
}
