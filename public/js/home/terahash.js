
$(document).ready(function(){
    $.ajax({
        type:"GET",
        url:"./terahash/getPayouts",
        dataType:"json",
        success: function(json){

            var data = [];
            var labels = [];
            for (let i = 0; i < json.length; i++) {
                 data.push(Number(json[i].monthly_payout).toFixed(7));
                 labels.push(json[i].month);
            }

            var barData = {
            labels: labels,
            datasets: [
                {
                    label: "Ganancia",
                    backgroundColor: 'rgba(119,186,227, 0.8)',
                    pointBorderColor: "#fff",
                    data: data
                }
            ]
            };
        
            var barOptions = {
                responsive: true
            };
        
        
            var ctx2 = document.getElementById("ThRevenue").getContext("2d");
            new Chart(ctx2, {type: 'bar', data: barData, options:barOptions});

        },
        error :function(json){

        },
        complete: function(json){

        }
    })
})

/**
 * Append data to Terahash vs Time graph
 */
function thVsTime(value){

    seconds++;
    thVsTimeChart.data.labels.push(seconds);
    thVsTimeChart.data.datasets.forEach((dataset) => {
        if(dataset['data'].length > 20){
            thVsTimeChart.data.labels.shift();
            dataset.data.shift();
        }
        dataset.data.push(value);
    });
    thVsTimeChart.update();
}

/**
 * Function that calls api
 */
function terahash(){

    $.ajax({
        type:"GET",
        url:"./terahash/get",
        dataType:"json",
        success: function(json){
            var terahash = Number(json.terahash).toFixed(2);
            $('#terahash_balance').html(`${terahash} TH/s`);
            thVsTime(terahash);
        },
        error :function(json){

        },
        complete: function(json){

        }
    })
    
}

setInterval(function(){
    terahash();
},5000);