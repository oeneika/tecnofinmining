-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2018 at 10:32 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tecnofinmining`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `payment_method_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(180) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`payment_method_id`, `name`) VALUES
(1, 'Visa'),
(2, 'Master'),
(3, 'BTC'),
(4, 'Balance');

-- --------------------------------------------------------

--
-- Table structure for table `payouts`
--

CREATE TABLE `payouts` (
  `payout_id` bigint(255) NOT NULL,
  `payout` float NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` bigint(255) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `payouts`
--

INSERT INTO `payouts` (`payout_id`, `payout`, `created_at`, `updated_at`, `user_id`) VALUES
(1, 0.06, '2018-09-06 00:00:00', '2018-09-06 00:00:00', 1),
(2, 1, '2018-10-07 00:00:00', '2018-10-07 00:00:00', 1),
(5, 0.05, '2018-10-25 00:00:00', '2018-10-25 00:00:00', 1),
(6, 0.007, '2018-08-01 00:00:00', '2018-08-01 00:00:00', 1),
(7, 1, '2018-08-15 00:00:00', '2018-08-15 00:00:00', 1),
(8, 0.7, '2018-11-02 00:00:00', '2018-11-02 00:00:00', 1),
(9, 0.00000919242, '2018-11-07 17:32:52', '2018-11-07 17:32:52', 1),
(10, 0.00000919242, '2018-11-07 17:34:00', '2018-11-07 17:34:00', 1),
(11, 0.000735394, '2018-11-07 17:34:01', '2018-11-07 17:34:01', 1),
(12, 0.000000735394, '2018-11-07 17:34:01', '2018-11-07 17:34:01', 1),
(13, 0.00000919091, '2018-11-07 17:35:02', '2018-11-07 17:35:02', 1),
(14, 0.000735273, '2018-11-07 17:35:03', '2018-11-07 17:35:03', 1),
(15, 0.000000735273, '2018-11-07 17:35:04', '2018-11-07 17:35:04', 1);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` bigint(255) UNSIGNED NOT NULL,
  `user_id` bigint(255) UNSIGNED NOT NULL,
  `amount` float UNSIGNED NOT NULL,
  `terahash_quantity` float UNSIGNED NOT NULL,
  `payment_method_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `user_id`, `amount`, `terahash_quantity`, `payment_method_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0.0133333, 1, '2018-09-05 00:00:00', '2018-11-05 23:06:11'),
(2, 1, 300, 4, 1, '2018-11-06 02:09:57', '2018-11-06 02:09:57'),
(3, 1, 600, 8, 1, '2018-11-06 02:13:50', '2018-11-06 02:13:50'),
(4, 4, 600, 8, 1, '2018-10-06 16:01:02', '2018-11-06 16:01:02'),
(5, 1, 75, 1, 4, '2018-11-07 17:05:15', '2018-11-07 17:05:15'),
(6, 1, 6000, 80, 1, '2018-11-07 17:06:50', '2018-11-07 17:06:50'),
(12, 1, 6, 0.08, 4, '2018-11-07 17:31:41', '2018-11-07 17:31:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(255) UNSIGNED NOT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `terahash_balance` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `lastname`, `phone`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `terahash_balance`) VALUES
(1, '', 'admin', '', '', 'admin@gmail.com', NULL, '$2y$10$0EDK9oSICwM/Mo7W11s9O.0ANLEQ/oEdfjJt/1qUXjXy8giqE3lUK', NULL, '2018-11-06 01:29:16', '2018-11-07 22:07:31', 50.7593),
(4, '', 'peter_parker', '', '', 'friendly@neighborhood.com', NULL, '$2y$10$H3XxFdbDeAHATYkIC/7hkO6o3ATq9QpKvitWnfnvSmjfCm/YotwJK', NULL, '2018-11-06 19:58:51', '2018-11-06 20:01:02', 8),
(5, 'philopator', 'Cleopatra', 'Philopator', '0212345653', 'philopator@ptolomy.com', NULL, '$2y$10$mpQtkghWGlz6UziF0i51YOnMVbI3XcwVmgprnsvuEckvm//L16mM6', 'RoBQLffrwlHDoZbTjs6QaItvM7KmhWx1a3BEofXZgZDsE60JGEiM2dCZLSJN', '2018-11-08 01:01:21', '2018-11-08 01:27:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `wallet_id` bigint(255) UNSIGNED NOT NULL,
  `user_id` bigint(255) UNSIGNED NOT NULL,
  `label` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(37) COLLATE utf8_bin NOT NULL,
  `current_balance` float NOT NULL DEFAULT '0',
  `historic_balance` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`wallet_id`, `user_id`, `label`, `address`, `current_balance`, `historic_balance`) VALUES
(1, 4, 'BTC', '$2y$10$K/9jsNhcoAqy0J5H.pxzcuikZrWFRP', 0, 0),
(2, 1, 'BTC', '$2y$10$K/9jsNhcoAqy0J5H.pxzcuikZrWFR4', 0.00228315, 0.00320238),
(3, 5, 'BTC', '$2y$10$MmGI4urM4T2G3SIFOkV7gOTuJsNFF.', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`payment_method_id`);

--
-- Indexes for table `payouts`
--
ALTER TABLE `payouts`
  ADD PRIMARY KEY (`payout_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `payment_method_id` (`payment_method_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`wallet_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `payment_method_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payouts`
--
ALTER TABLE `payouts`
  MODIFY `payout_id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `wallet_id` bigint(255) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `payouts`
--
ALTER TABLE `payouts`
  ADD CONSTRAINT `payouts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`payment_method_id`);

--
-- Constraints for table `wallets`
--
ALTER TABLE `wallets`
  ADD CONSTRAINT `wallets_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
