@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Balance
            </h5>
        </div>
        <div class="ibox-content">
            <div class="row">
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Total de BTC </th>
                        <th>Total en Dólares </th>
                        <th>Fecha </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ round( Auth::user()->wallet->current_balance, 8) }} BTC</td>
                        <td>{{ round( Auth::user()->wallet->current_balance * $BTC['bpi']['USD']['rate_float'], 2)  }}$</td>
                        <td>{{ date('M d, Y') }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
@endsection
