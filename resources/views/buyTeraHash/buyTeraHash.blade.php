@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Compra de terahash
                    </h5>
                </div>
                <div class="ibox-content">
                    <form id="store_buy_form">
                    {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Forma de pago</label>
                                    <select name="payment_method" id="" class="form-control">
                                        
                                        @foreach ($methods as $method)
                                            <option value="{{ $method->payment_method_id }}">{{ $method->name }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Monto</label>
                                    <input name="amount" type="number" id="amount" class="form-control" placeholder="0.00">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Equivalencia en TH</label>
                                    <input name="terahash_quantity" type="number" id="quantity" class="form-control" disabled="" placeholder="0.00">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary mg-30" id="store_buy_btn" style="width: 100%;">Realizar compra</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_section')
    <script src="{{ asset('js/buyTeraHash/store.js') }}"></script>
    <script src="{{ asset('js/buyTeraHash/calculator.js') }}"></script>
@endsection
