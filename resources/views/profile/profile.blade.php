@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Perfil de Usuario
            </h5>
        </div>
        <div class="ibox-content">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="mt-sm mb-xs"></h3>
                                <p><strong>Nombre y Apellido: </strong>{{ Auth::user()->name }} {{
                                    Auth::user()->lastname }}</p>
                                <p><strong>E-mail: </strong><a href="mailto:#"> {{ Auth::user()->email }}</a></p>
                                <p><strong>Usuario: </strong>{{ Auth::user()->username }}</p>
                                <hr>
                                <p><strong>Teléfono Local: </strong> {{ Auth::user()->phone }}</p>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- begin panel -->
                <div class="panel panel-inverse" style="width: 100%">
                    <div class="panel-heading">

                        <h4 class="panel-title">Completar perfil de usuario</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="user-form" method="post" data-parsley-priority-enabled="false" type='POST'
                                    action="{{ route('editUser',['id'=>Auth::user()->id]) }}">
                                    {{ csrf_field() }}
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="username" class="control-label">Usuario</label>
                                                <input type="text" name="username" class="form-control" placeholder="Introduce tu usuario"
                                                    value="{{ Auth::user()->username }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="address">Nombre</label>
                                                <input type="text" id="name" name="name" placeholder="" class="form-control"
                                                    value="{{ Auth::user()->name }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="address">Apellido</label>
                                                <input type="text" id="lastname" name="lastname" placeholder="" class="form-control"
                                                    value="{{ Auth::user()->lastname }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="address">E-mail</label>
                                                <input type="text" id="email" name="email" placeholder="" class="form-control"
                                                    value="{{ Auth::user()->email }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label" for="phone">Teléfono local</label>
                                                <input type="text" id="phone" name="phone" placeholder="" class="form-control"
                                                    value="{{ Auth::user()->phone }}">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-success">Actualizar Perfil</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel -->
            </div>
        </div>
    </div>

</div>
@section('footer_section')
@endsection
@endsection
