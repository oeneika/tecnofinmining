<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Animation CSS -->
        <link href="{{ asset('template/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('template/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">

    </head>
    <body class="gray-bg background">
        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>
                    <h1 class="logo-name"><img src="template/img/myart/1.png" alt="" class="width300"></h1>
                </div>
                <h3>Bienvenido a Tecnofinmining</h3>

                <form class="m-t" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contrase&ntilde;a" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">{{ __('Iniciar sesión') }}</button>

                    <a class="btn btn-link" href="{{ route('password.request') }}"><small>{{ __('¿Olvidó su contraseña?') }}</small></a>
                    <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">REGISTRARSE</a>
                </form>
            </div>
        </div>


        <!-- Mainly scripts -->
        <script src="{{ asset('template/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('template/js/popper.min.js') }}"></script>
        <script src="{{ asset('template/js/bootstrap.js') }}"></script>
    </body>
</html>
