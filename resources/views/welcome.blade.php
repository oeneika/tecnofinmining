<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Animation CSS -->
        <link href="{{ asset('template/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('template/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">

    </head>
    <body id="page-top" class="landing-page no-skin-config">
        <div class="navbar-wrapper">
            <nav class="navbar navbar-default navbar-fixed-top navbar-expand-md" role="navigation">
                <div class="container">
                    <a class="navbar-brand" href="#"><img src="template/img/myart/1.png" class="width100" alt=""></a>
                    <div class="navbar-header page-scroll">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse justify-content-end" id="navbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="nav-link page-scroll" href="#page-top">Inicio</a></li>
                            <li><a class="nav-link page-scroll" href="#features">Quiénes somos</a></li>
                            <li><a class="nav-link page-scroll" href="#team">Servicios</a></li>
                            <li><a class="nav-link page-scroll" href="#contact">Contacto</a></li>
                            <li><a class="nav-link page-scroll" href="{{ route('login') }}">Iniciar sesión</a></li>
                            <li><a class="nav-link page-scroll" href="{{ route('register') }}">Registrarse</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div id="inSlider" class="carousel slide" data-ride="carousel" >
            <ol class="carousel-indicators">
                <li data-target="#inSlider" data-slide-to="0" class="active"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Somos Tecnofinmining<br/>
                                compra Terahash <br/>
                                y ten un rendimiento mensual<br/>
                                de tu dinero</h1>
                            <p>Más seguro que un banco, más rentable también.</p>
                            <p>
                                <a class="btn btn-lg btn-primary" href="{{ route('login') }}" role="button">Conviértete en Minero hoy mismo</a>
                            </p>
                        </div>
                    </div>
                    <!-- Set background for slide in css -->
                    <div class="header-back one"></div>
                    <div id="particles-js"></div>

                </div>
            </div>
        </div>


        

        <section  class="container features">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="navy-line"></div>
                    <h1>¿Quiénes somos?<br/> <span class="navy"> </span> </h1>
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center wow fadeInLeft">
                    <div>
                        <i class="fa fa-mobile features-icon"></i>
                        <h2>Full responsive</h2>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
                    </div>
                    <div class="m-t-lg">
                        <i class="fa fa-bar-chart features-icon"></i>
                        <h2>6 Charts Library</h2>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
                    </div>
                </div>
                <div class="col-md-6 text-center  wow zoomIn">
                    <img src="template/img/landing/perspective.png" alt="dashboard" class="img-fluid">
                </div>
                <div class="col-md-3 text-center wow fadeInRight">
                    <div>
                        <i class="fa fa-envelope features-icon"></i>
                        <h2>Mail pages</h2>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
                    </div>
                    <div class="m-t-lg">
                        <i class="fa fa-google features-icon"></i>
                        <h2>AngularJS version</h2>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="team" class="container services">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="navy-line"></div>
                    <h1>Nuestros servicios</h1>
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. </p>
                </div>
            </div>
            <div class="row features-block">
                <div class="col-lg-6 features-text wow fadeInLeft">
                    <small>INSPINIA</small>
                    <h2>Perfectly designed </h2>
                    <p>INSPINIA Admin Theme is a premium admin dashboard template with flat design concept. It is fully responsive admin dashboard template built with Bootstrap 3+ Framework, HTML5 and CSS3, Media query. It has a huge collection of reusable UI components and integrated with latest jQuery plugins.</p>
                </div>
                <div class="col-lg-6 text-right wow fadeInRight">
                    <img src="template/img/landing/dashboard.png" alt="dashboard" class="img-fluid float-right">
                </div>
            </div>
        </section>


        <section id="contact" class="gray-section contact">
            <div class="container">
                <div class="row m-b-lg">
                    <div class="col-lg-12 text-center">
                        <div class="navy-line"></div>
                        <h1>Contact Us</h1>
                        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod.</p>
                    </div>
                </div>
                <div class="row m-b-lg justify-content-center">
                    <div class="col-lg-3 ">
                        <address>
                            <strong><span class="navy">Company name, Inc.</span></strong><br/>
                            795 Folsom Ave, Suite 600<br/>
                            San Francisco, CA 94107<br/>
                            <abbr title="Phone">P:</abbr> (123) 456-7890
                        </address>
                    </div>
                    <div class="col-lg-4">
                        <p class="text-color">
                            Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <a href="mailto:test@email.com" class="btn btn-primary">Send us mail</a>
                        <p class="m-t-sm">
                            Or follow us on social platform
                        </p>
                        <ul class="list-inline social-icon">
                            <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 text-center m-t-lg m-b-lg">
                        <p><strong>&copy; 2015 Company Name</strong><br/> consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
                    </div>
                </div>
            </div>
        </section>


        <!-- Mainly scripts -->
        <script src="{{ asset('template/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('template/js/popper.min.js') }}"></script>
        <script src="{{ asset('template/js/bootstrap.js') }}"></script>
        <script src="{{ asset('template/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{ asset('template/js/inspinia.js') }}"></script>
        <script src="{{ asset('template/js/plugins/pace/pace.min.js') }}"></script>
        <script src="{{ asset('template/js/plugins/wow/wow.min.js') }}"></script>
        <script src="{{ asset('template/js/plugins/particles.js-master/particles.js') }}"></script>
        <script src="{{ asset('template/js/plugins/particles.js-master/demo/js/app.js') }}"></script>

        <script>
        $(document).ready(function () {

            $('body').scrollspy({
                target: '#navbar',
                offset: 80
            });

            // Page scrolling feature
            $('a.page-scroll').bind('click', function(event) {
                var link = $(this);
                $('html, body').stop().animate({
                    scrollTop: $(link.attr('href')).offset().top - 50
                }, 500);
                event.preventDefault();
                $("#navbar").collapse('hide');
            });
        });

        var cbpAnimatedHeader = (function() {
            var docElem = document.documentElement,
                    header = document.querySelector( '.navbar-default' ),
                    didScroll = false,
                    changeHeaderOn = 200;
            function init() {
                window.addEventListener( 'scroll', function( event ) {
                    if( !didScroll ) {
                        didScroll = true;
                        setTimeout( scrollPage, 250 );
                    }
                }, false );
            }
            function scrollPage() {
                var sy = scrollY();
                if ( sy >= changeHeaderOn ) {
                    $(header).addClass('navbar-scroll')
                }
                else {
                    $(header).removeClass('navbar-scroll')
                }
                didScroll = false;
            }
            function scrollY() {
                return window.pageYOffset || docElem.scrollTop;
            }
            init();
        })();

        // Activate WOW.js plugin for animation on scrol
        new WOW().init();
        </script>
    </body>
</html>
