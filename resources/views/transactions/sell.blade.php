@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content">
    <div class="ibox ">
        <div class="ibox-title">
            <h5>Compras
            </h5>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>

                        <th>#</th>
                        <th>Tipo </th>
                        <th>Retiro </th>
                        <th>Terahash </th>
                        <th>Fecha de transacción </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($sells as $sell)
                        <tr>
                            <td>{{ $sell->transaction_id }}</td>
                            <td>SHA-256</small></td>
                            <td>{{ number_format($sell->amount,2,',','.') }}$</td>
                            <td>{{ $sell->terahash_quantity }}</td>
                            <td>{{ $sell->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
</div>
@endsection
