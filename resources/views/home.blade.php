@extends('layouts.app')

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Terahash vs tiempo
                    </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="ThVsTime" height="140"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Cantidad de dinero producido por mes</h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="ThRevenue" height="140"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="widget red-bg p-lg text-center">
                <div class="m-b-md text-white">
                    <i class="fa fa-tasks fa-4x"></i>
                    <i class="fa fa-bolt fa-4x"></i>
                    <h1 class="m-xs" id="terahash_balance">{{ round(Auth::user()->terahash_balance, 2) }} TH/s</h1>
                    <h3 class="font-bold no-margins">
                    </h3>
                    <small><i class="fa fa-bolt" style="font-size: 7px;"></i>HASHRATE</small>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Pronóstico de ganancias mensuales</h5>
                </div>
                <div class="ibox-content">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Conversión</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">{{ round( ( Auth::user()->wallet->current_balance + ( Auth::user()->terahash_balance * 0.06 ) ) / ($BTC['bpi']['USD']['rate_float'] ), 8 ) }} BTC = {{ number_format( Auth::user()->wallet->current_balance + ( Auth::user()->terahash_balance * 0.06 ), 2, ',' , '.' ) }} USD</th>
                          <td>1 mes</td>
                        </tr>
                        <tr>
                          <th scope="row">{{ round( ( ( Auth::user()->terahash_balance * 0.06 ) + ( Auth::user()->terahash_balance * 0.06 * 2 ) ) / $BTC['bpi']['USD']['rate_float'], 8 ) }} BTC = {{ number_format( ( Auth::user()->terahash_balance * 0.06 ) + ( Auth::user()->terahash_balance * 0.06 * 2 ), 2, ',', '.' ) }} USD</th>
                          <td>2 meses</td>
                        </tr>
                        <tr>
                          <th scope="row">{{ round( ( ( Auth::user()->terahash_balance * 0.06 * 2 ) + ( Auth::user()->terahash_balance * 0.06 * 3 ) ) / $BTC['bpi']['USD']['rate_float'], 8 ) }} BTC = {{ number_format( ( Auth::user()->terahash_balance * 0.06 * 2 ) + ( Auth::user()->terahash_balance * 0.06 * 3 ), 2, ',', '.' ) }} USD</th>
                          <td>3 meses</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="widget bg-blue p-lg text-center">
                <div class="m-b-md text-white">
                    <h1 class="m-xs">{{ round( Auth::user()->wallet->current_balance, 8 ) }} BTC</h1>
                    <h3 class="font-bold no-margins">
                        Balance general
                    </h3>                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="widget bg-blue p-lg text-center">
                <div class="m-b-md text-white">
                    <h1 class="m-xs">{{ round( Auth::user()->wallet->historic_balance, 8 ) }} BTC</h1>
                    <h3 class="font-bold no-margins">
                        Total de btc a la fecha
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="widget bg-blue p-lg text-center">
                <div class="m-b-md text-white">
                    <h1 class="m-xs">{{ number_format( Auth::user()->wallet->current_balance * $BTC['bpi']['USD']['rate_float'], 2, ',', '.' ) }} $</h1>
                    <h3 class="font-bold no-margins">
                        Equivalencia en Dólares
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_section')
<script>
    var thVsTimeChart;
    var seconds = 1;
</script>
<script>
        var barData = {
        labels: [1],
        datasets: [
            {
                label: "TH/s",
                backgroundColor: 'rgba(1, 53, 83, 0.8)',
                pointBorderColor: "#fff",
                data: [{{ Auth::user()->terahash_balance }}]
            }
        ]
    };

    var barOptions = {
        responsive: true
    };


    var ctx2 = document.getElementById("ThVsTime").getContext("2d");
    thVsTimeChart = new Chart(ctx2, {type: 'line', data: barData, options:barOptions});
</script>
<script>
        
</script>
<script src="{{ asset('js/home/terahash.js') }}"></script>
@endsection
