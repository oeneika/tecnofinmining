<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Animation CSS -->
        <link href="{{ asset('template/css/animate.css') }}" rel="stylesheet">
        <link href="{{ asset('template/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

        <!-- Morris -->
        <link href="{{ asset('template/css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">

        {{-- Toastr --}}
        <link href="{{ asset('template/js/toastr/build/toastr.min.css') }}" rel="stylesheet"/>

        <!-- Custom styles for this template -->
        <link href="{{ asset('template/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/estilos.css') }}" rel="stylesheet">


        @yield('header_section')

    </head>
    <body>
        <div id="wrapper">
            @include('layouts.menu')

            <div id="page-wrapper" class="gray-bg">
                @include('layouts.header')
                @yield('content')
                @include('layouts.footer')
            </div>
        </div>
    
        <!-- Mainly scripts -->
        <script src="{{ asset('template/js/jquery-3.1.1.min.js') }}"></script>
        <script src="{{ asset('template/js/popper.min.js') }}"></script>
        <script src="{{ asset('template/js/bootstrap.js') }}"></script>
        <script src="{{ asset('template/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
        <script src="{{ asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- Custom and plugin javascript -->
        <script src="{{ asset('template/js/inspinia.js') }}"></script>
        <script src="{{ asset('template/js/plugins/pace/pace.min.js') }}"></script>

        <!-- Flot -->
        <script src="{{ asset('template/js/plugins/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('template/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('template/js/plugins/flot/jquery.flot.spline.js') }}"></script>
        <script src="{{ asset('template/js/plugins/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('template/js/plugins/flot/jquery.flot.pie.js') }}"></script>
        <script src="{{ asset('template/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
        <script src="{{ asset('template/js/plugins/flot/curvedLines.js') }}"></script>

        <!-- ChartJS-->
        <script src="{{ asset('template/js/plugins/chartJs/Chart.min.js') }}"></script>

        <!-- Peity -->
        <script src="{{ asset('template/js/plugins/peity/jquery.peity.min.js') }}"></script>

        <!-- Peity demo -->
        <script src="{{ asset('template/js/demo/peity-demo.js') }}"></script>


        <script src="{{ asset('template/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('template/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
        <script src="{{ asset('template/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
        <script src="{{ asset('template/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('template/js/demo/sparkline-demo.js') }}"></script>
        <script src="{{ asset('template/js/plugins/chartJs/Chart.min.js') }}"></script>
        <script src="{{ asset('template/js/demo/chartjs-demo.js') }}"></script>
        
        <script src="{{ asset('template/js/toastr/build/toastr.min.js') }}"></script>
        @yield('footer_section')
    </body>
</html>
