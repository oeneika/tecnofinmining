<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

#Home
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/terahash/get','HomeController@simulation')->name('simulationTerahash')->middleware('auth');
Route::get('/terahash/getPayouts','HomeController@hashRevenue')->name('hashRevenue')->middleware('auth');

#Buy terahash
Route::get('/comprarterash', 'BuyTeraHashController@index')->name('buyTeraHash')->middleware('auth');

#Transactions
Route::get('/historialcompras', 'TransactionsController@buyHistory')->name('historialcompras')->middleware('auth');
Route::get('/historialventas', 'TransactionsController@sellHistory')->name('historialventas')->middleware('auth');
Route::post('/transaction/store', 'TransactionsController@store')->name('storeTransaction')->middleware('auth');

#Balance
Route::get('/balance', 'BalanceController@index')->name('balance')->middleware('auth');

#Profile
Route::get('perfil','ProfilesController@index')->name('perfil')->middleware('auth');
Route::put('editUser/{id}','ProfilesController@edit')->name('editUser');
