<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#Registro y Login
Route::post('/login', 'PublicAuthController@login');
Route::post('/register', 'PublicAuthController@register');
Route::put('/update/{id}', 'PublicAuthController@update');

#Transacciones
Route::post('/transaction/store', 'TransactionsController@store');
Route::get('/transaction/buy_history', 'TransactionsController@buyHistory');

#Dashboard
Route::get('/terahash/get', 'HomeController@simulation');
Route::get('/terahash/getPayouts','HomeController@hashRevenue');
Route::get('/btc','HomeController@btcPrice');

#User info
Route::get('/user/{id}', 'UserController@show');
Route::get('/user/transactions/{id}', 'UserController@getTransactions');

#Payment Methods
Route::get('/payment_method', 'PaymentMethodController@get');

